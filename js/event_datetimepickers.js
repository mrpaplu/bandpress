/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function(){
   jQuery("#startDatePicker").kendoDatePicker({
       format: "dd-MM-yyyy"
   }); 
   jQuery("#startTimePicker").kendoTimePicker({
       format: "HH:mm"
   });
   jQuery("#endDatePicker").kendoDatePicker({
       format: "dd-MM-yyyy"
   }); 
   jQuery("#endTimePicker").kendoTimePicker({
       format: "HH:mm"
   });
});