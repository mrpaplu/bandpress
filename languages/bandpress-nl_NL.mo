��    -      �      �      �     �     �  
     
                   3  
   <     G     K  	   T     ^     d     u     |  &   �  (   �  5   �  /        C     c  	   l     v     y     �     �     �  #   �     �     �     	  2   #  3   V  2   �     �  
   �     �     �     �  G        P     V  
   f     q  �   u     \     l     �     �     �     �  	   �     �     �  
   �     �  	      
   
          !     ;      Y  1   z  <   �     �     �     	     	  *   	     E	     X	     v	     �	     �	     �	     �	  =   �	  A   /
  <   q
     �
     �
     �
     �
     �
           )     /     C     T   Add New Add New Event All Events All events Author Bandpress settings Category Edit Event End End date Entry fee Event Event properties Events Events not found Expecting a Numeric value! Please fix. Expecting comma separated numeric values Expecting comma separated numeric values! Please fix. How many events should be shown in the archive? Invalid email! Please re-enter! Location New Event No No events found in trash Number of events Number of events per page: Past events Please enter a valid email address. Save Changes Search Events Settings for this section Should end dates be supported in Bandpress events? Should entry fees be supported in Bandpress events? Should locations be supported in Bandpress events? Start Start date Support end dates? Support entry fees? Support locations? This setting field cannot be empty! Please enter a valid email address. Title Upcoming events View Event Yes Project-Id-Version: Bandpress v0.8
PO-Revision-Date: 2014-03-06 00:09:27+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: CSL v1.x Nieuw evenement Nieuw evenement toevoegen Alle evenementen Alle evenementen Auteur Bandpress instellingen Categorie Evenement bewerken Einde Eind datum Entreeprijs Evenement Voorkeuren Evenementen Evenementen niet gevonden Voer een numerieke waarde in! Vul komma-gescheiden waarden in! Vul numerieke waarden, met komma's gescheiden in! Hoeveel evenementen moeten er zichtbaar zijn in het archief? Ongeldig e-mailadres Locatie Nieuw evenement Nee Geen evenementen gevonden in de prullenbak Aantal evenementen Aantal evenementen per pagina Afgelopen evenementen Vul een geldig e-mail adres in! Wijzigingen opslaan Evenementen zoeken Voorkeuren voor dit onderdeel Moeten eind data ondersteund worden in Bandpress evenementen? Moeten entreeprijzen ondersteund worden in Bandpress evenementen? Moeten locaties ondersteund worden in Bandpress evenementen? Start Start datum Ondersteun eind datum? Entreeprijs ondersteunen? Locaties ondersteunen? E-mail adres mag niet leeg zijn! Titel Komende evenementen Bekijk evenement Ja 