<?php
/*
Plugin Name: Bandpress
Plugin URI: http://tumblersedge.nl/
Description: Event management plugin for Wordpress.
Version: 0.9
Author: Kay van Bree
Author URI: http://tumblersedge.nl/
*/

if(!class_exists('Bandpress')){
class Bandpress {
    public function __construct(){
        add_action('admin_enqueue_scripts', array($this, 'register_admin_scripts'));
        add_action('wp_enqueue_scripts', array($this, 'register_styles'));
        
        $this->include_components();
        $this->initialize_update_checker();
        $this->initialize_components();
        $this->register_widgets();
    }
    
    public function include_components(){
        // Bandpress
        include_once('includes/bp-categories.php');
        include_once('includes/bp-events.php');
        include_once('includes/bp-localization.php');
        include_once('includes/bp-menus.php');
        include_once('includes/bp-options.php');
        include_once('includes/bp-upcoming-events-widget.php');
        include_once('includes/bp-events-shortcode.php');
        
        // Lib
        include_once('lib/datetime52.php');
        include_once('lib/plugin-updates/plugin-update-checker.php');
    }
    
    public function initialize_components(){
        new BPLocalization();
        new BPOptions();
        new BPEvents();
        new BPEventCategories();
        new BPMenus();
        new BPEventsShortcode();
    }
    
    public function register_widgets(){
        add_action('widgets_init', 'bp_register_upcoming_shows_widget');
    }
    
    public function register_styles(){
        wp_enqueue_style('upcoming_events_widget', plugin_dir_url(__FILE__) . 'css/upcoming-events-widget.css');
    }
    
    public function register_admin_scripts(){
        // Styles
        wp_enqueue_style('kendoui_common_style', plugin_dir_url(__FILE__) . 'lib/kendoui/styles/kendo.common.min.css');
        wp_enqueue_style('kendoui_default_style', plugin_dir_url(__FILE__) . 'lib/kendoui/styles/kendo.bootstrap.min.css');
        wp_enqueue_style('bp_datetimepicker_style', plugin_dir_url( __FILE__ ) . 'css/bp-datetimepicker-style.css');
    
        // Scripts
        wp_enqueue_script('kendoui', plugin_dir_url(__FILE__) . 'lib/kendoui/js/kendo.web.min.js', array('jquery'));
        wp_enqueue_script('datetimepickers', plugin_dir_url(__FILE__) . 'js/event_datetimepickers.js', array('jquery'));
    }
    
    public function initialize_update_checker(){
        new PluginUpdateChecker(
            'https://bitbucket.org/mrpaplu/bandpress/downloads/update.json',
            __FILE__,
            'bandpress'
        );
    }
}
}

new Bandpress();
?>