<?php
/**
 * A widget for upcoming BPEvents
 *
 * @author Kay van Bree
 */
if(!function_exists('bp_register_upcoming_shows_widget')){
function bp_register_upcoming_shows_widget(){
    register_widget('BPUpcomingEventsWidget');
}
}

if(!class_exists('BPUpcomingEventsWidget')){
class BPUpcomingEventsWidget extends WP_Widget {
    function BPUpcomingEventsWidget(){
        $widget_ops = array(
            'classname' => 'bp-upcoming-events-widget',
            'description' => 'Shows your upcoming events'
        );
        $control_ops = array(
            'id_base' => 'bp-upcoming-events-widget'
        );
        $this->WP_Widget(
            'bp-upcoming-events-widget',
            __('Upcoming events', 'bandpress'),
            $widget_ops,
            $control_ops
        );
    }
    
    function form ($instance) {
      $defaults = array('numberposts' => '5','title'=>'');
      $instance = wp_parse_args( (array) $instance, $defaults ); 
?>

      <p>
         <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __('Title', 'bandpress'); ?>:</label>
         <input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo $instance['title'] ?>" size="20"> </p>

      <p>
       <label for="<?php echo $this->get_field_id('numberposts'); ?>"><?php echo __('Number of events', 'bandpress'); ?>:</label>
       <select id="<?php echo $this->get_field_id('numberposts'); ?>" name="<?php echo $this->get_field_name('numberposts'); ?>">
<?php 
        for ($i=5;$i<=40;$i+=5) {
            echo '<option value="'.$i.'"';
            if ($i==$instance['numberposts']) echo ' selected="selected"';
            echo '>'.$i.'</option>';
         } 
?>
        </select>
      </p>
<?php
    }
    
    function update($new_instance, $old_instance){
        $instance = $old_instance;
        
        $instance['numberposts'] = $new_instance['numberposts'];
        $instance['title'] = $new_instance['title'];
        
        return $instance;
    }
    
    function widget($args, $instance){
        extract($args);
        
        $title = $instance['title'];
        $numberposts = $instance['numberposts'];
        
        echo $before_widget;
        echo $before_title.$title.$after_title;
        
        global $wpdb;
        
        $now = date('YmdHi');
        $now -= 5000; // Half dagje erbij dan maar?
        
        $you = new WP_Query( array(
            'post_type' => 'event',
            'posts_per_page' => $numberposts,
            'meta_query' => array(
                array(
                    'key' => '_start_event_datetime',
                    'value' => $now,
                    'type' => 'numeric',
                    'compare' => '>'
                )
            ),
            'orderby' => 'meta_value',
            'meta_key' => '_start_event_datetime',
            'order' => 'ASC'
        ));
        
        if($you->have_posts()){
            echo '<div class="upcoming-events">';
            while($you->have_posts()){
                $you->the_post();
                $datetime = BPEvents::get_event_date(); 
                $day = date_i18n( "j", $datetime->getTimeStamp());
                $month = date_i18n( "M", $datetime->getTimeStamp());
                $out .= '<li><a href="'. get_permalink() .'">'. $day . " " . $month . " " . get_the_title() .'</a></li>';
?>
        <a href="<?php the_permalink(); ?>" class="upcoming-event">
            <div class="upcoming-event-column">
                <div class="upcoming-event-datetime">
                    <?php echo $day; ?><br />
                    <?php echo $month; ?>
                </div>
            </div>
            <div class="upcoming-event-column upcoming-event-title">
                <?php the_title(); ?>
            </div>
        </a>
<?php
            }
            echo "</div>";
        } else {
            echo 'No events found';
        }
        echo $after_widget;
    }
}
}
?>