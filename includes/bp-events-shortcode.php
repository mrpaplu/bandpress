<?php
/*
 * The [events] shortcode
 */

if(!class_exists('BPEventsShortcode')){
class BPEventsShortcode {
    public function __construct(){
        add_shortcode('events', array(&$this, 'events_shortcode'));
    }
    
    // [events ]
    public function events_shortcode($atts){
        extract( shortcode_atts( array(
            'perpage' => 10
        ), $atts));
        
        global $wpdb;
        
        $now = date('YmdHi');
        $now -= 5000; // Half dagje erbij dan maar?
        
        $you = $this->getQuery($perpage);
        
        if($you->have_posts()){
            echo '<div class="upcoming-events">';
            while($you->have_posts()){
                $you->the_post();
                $datetime = BPEvents::get_event_date(); 
                $day = date_i18n( "j", $datetime->getTimeStamp());
                $month = date_i18n( "M", $datetime->getTimeStamp());
                $out .= '<li><a href="'. get_permalink() .'">'. $day . " " . $month . " " . get_the_title() .'</a></li>';
?>
        <a href="<?php the_permalink(); ?>" class="upcoming-event">
            <div class="upcoming-event-column">
                <div class="upcoming-event-datetime">
                    <?php echo $day; ?><br />
                    <?php echo $month; ?>
                </div>
            </div>
            <div class="upcoming-event-column upcoming-event-title">
                <?php the_title(); ?>
            </div>
        </a>
<?php
            }
            echo "</div>";
        } else {
            echo 'No events found';
        }
    }
    
    public function getQuery($perpage){
        $query = new WP_Query( array(
            'post_type' => 'event',
            'posts_per_page' => $perpage,
            'meta_query' => array(
                array(
                    'key' => '_start_event_datetime',
                    'value' => $now,
                    'type' => 'numeric',
                    'compare' => '>'
                )
            ),
            'orderby' => 'meta_value',
            'meta_key' => '_start_event_datetime',
            'order' => 'ASC'
        ));
        return $query;
    }
}
}
?>
