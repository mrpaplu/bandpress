<div class="wrap">
    <?php screen_icon('bandpress'); ?>
    <h2><?php echo $settings_output['bp_page_title']; ?></h2>  

    <form action="options.php" method="post">  
        <?php   
        // http://codex.wordpress.org/Function_Reference/settings_fields  
        settings_fields($settings_output['bp_option_name']);   

        // http://codex.wordpress.org/Function_Reference/do_settings_sections  
        do_settings_sections($settings_output['bp_option_slug']);   
        ?>  
        <p class="submit">  
            <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes','bandpress'); ?>" />  
        </p>  

    </form>  
</div><!-- wrap -->  