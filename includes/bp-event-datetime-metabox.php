<?php
/*
 * The html for the DateTime metabox of event
 */
?>
<table>
    <tr>
        <td><label for="startDatePicker"><?php _e("Start", 'bandpress'); ?></label></td>
        <td><input id="startDatePicker" name="_start_date" value="<?php echo $date; ?>" /></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><input id="startTimePicker" name="_start_time" value="<?php echo $time; ?>" /></td>
        <td></td>
    </tr>
<?php if($options['use_end_date'] == "yes"): ?>
    <tr>
        <td><label for="endDatePicker"><?php _e("End", 'bandpress'); ?></label></td>
        <td><input id="endDatePicker" name="_end_date" value="<?php echo $enddate; ?>" /></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><input id="endTimePicker" name="_end_time" value="<?php echo $endtime; ?>" /></td>
        <td></td>
    </tr>
<?php endif; ?>
<?php if($options['use_event_location'] == "yes"): ?>
    <tr>
        <td><label for="_event_location"><?php _e('Location', 'bandpress'); ?></label></td>
	<td><input type="text" name="_event_location" value="<?php echo $event_location; ?>" /></td>
        <td></td>
    </tr>
<?php endif; ?>
<?php if($options['use_entry_fee'] == "yes"): ?>
    <tr>
        <td><label for="_event_entry_fee"><?php _e('Entry fee', 'bandpress'); ?></label></td>
	<td><input type="text" name="_event_entry_fee" value="<?php echo $event_entry_fee; ?>" /></td>
        <td></td>
    </tr>
<?php endif; ?>
</table>