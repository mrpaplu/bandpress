<?php
/**
 * This class is responsible for the Bandpress functionality in the menu editor.
 *
 * @author Kay
 */
if(!class_exists('BPMenus')){
class BPMenus {
    public function __construct(){
        add_action( 'admin_head-nav-menus.php', array($this, 'add_menus_metabox'));
        add_filter( 'wp_get_nav_menu_items', array($this, 'get_menu_items'), 10, 3);
    }
    
    /**
     * Add the metabox
     */
    public function add_menus_metabox(){
        add_meta_box( 'bandpress-menus', __( 'Bandpress menus' ), array($this, 'menus_metabox'), 'nav-menus', 'side', 'default' );
    }
    
    /**
     * Take care of the front-office links
     * @param type $items
     * @param type $menu
     * @param type $args
     * @return type
     */
    public function get_menu_items($items, $menu, $args ) {
        /* alter the URL for cpt-archive objects */
        foreach ( $items as &$item ) {
            if ( $item->object != 'cpt-archive' ) continue;

            /* we stored the post type in the type property of the menu item */
            $item->url = get_post_type_archive_link( $item->type );

            if ( get_query_var( 'post_type' ) == $item->type ) {
              $item->classes []= 'current-menu-item';
              $item->current = true;
            }
        }

        return $items;
    }
    
    /**
     * Display the metabox
     */
    public function menus_metabox(){
        // Get post types
        $post_types = get_post_types( array( 'show_in_nav_menus' => true, 'has_archive' => true ), 'object' );
        
        /* hydrate the necessary properties for identification in the walker */
        foreach ( $post_types as &$post_type ) {
          $post_type->classes = array();
          $post_type->type = $post_type->name;
          $post_type->object_id = $post_type->name;
          $post_type->title = $post_type->labels->name;
          $post_type->object = 'cpt-archive';

          $post_type->menu_item_parent = null;
          $post_type->url = null;
          $post_type->xfn = null;
          $post_type->db_id = null;
          $post_type->target = null;
          $post_type->attr_title = null;
        }
        
        /* the native menu checklist */
        $walker = new Walker_Nav_Menu_Checklist( array() );
        
        ?>
        <div id="cpt-archive" class="posttypediv">
          <div id="tabs-panel-cpt-archive" class="tabs-panel tabs-panel-active">
          <ul id="ctp-archive-checklist" class="categorychecklist form-no-clear">
          <?php
            echo walk_nav_menu_tree( array_map('wp_setup_nav_menu_item', $post_types), 0, (object) array( 'walker' => $walker) );
          ?>
          </ul>
          </div><!-- /.tabs-panel -->
          </div>
          <p class="button-controls">
            <span class="add-to-menu">
                  <img class="waiting" src="<?php echo esc_url( admin_url( 'images/wpspin_light.gif' ) ); ?>" alt="" />
              <input type="submit"<?php disabled( $nav_menu_selected_id, 0 ); ?> class="button-secondary submit-add-to-menu" value="<?php esc_attr_e('Add to Menu'); ?>" name="add-ctp-archive-menu-item" id="submit-cpt-archive" />
            </span>
          </p>
        <?php
    }
}
}
?>
