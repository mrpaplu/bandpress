<?php
/**
 * BPEvents adds the Event post-type to Wordpress
 * 
 * Code used from https://github.com/devinsays/event-posts/blob/master/event-posts.php
 * @author Kay van Bree
 */
if(!class_exists('BPEvents')){
class BPEvents {
    public function __construct(){
        add_action('init', array($this, 'create_post_type'));
        add_action('admin_init', array($this, 'add_capabilities'));
        add_action('admin_init', array($this, 'add_date_metaboxes') );
        add_action('save_post', array($this, 'event_save_datetimepickers' ), 1, 2);
        add_action('pre_get_posts', array($this, 'event_query' ));
        add_filter('manage_edit-event_columns', array($this, 'add_columns'));
        add_action('manage_event_posts_custom_column', array($this, 'fill_columns'), 10, 2);
        add_filter("manage_edit-event_sortable_columns", array($this, 'add_sortable_columns'));
        add_filter('request', array($this, 'sort_columns') );
        add_filter( 'manage_users_columns', array($this, 'users_events_column' ));
        add_filter( 'manage_users_custom_column', array($this, 'user_events_column_value'), 10, 3 );
        //add_filter('pre_get_posts', array($this, 'events_in_loop'));
        add_action( 'restrict_manage_posts', array($this, 'events_filter_dropdown' ));
        add_filter( 'parse_query', array($this, 'events_filter') );
    }
    
    function events_filter( $query ){
        global $pagenow;
        $type = 'event';
        if($_GET['post_type'] != $type) return;
        
        if ($pagenow=='edit.php') {
            $filter = $_GET['EVENTS_TIME_FILTER'];
            $now = date('YmdHi');
            switch($filter){
                case 'upcoming':
                    $meta_query = array(
                        array(
                            'key' => '_start_event_datetime',
                            'value' => $now,
                            'type' => 'numeric',
                            'compare' => '>'
                        )
                    );
                    $query->set( 'meta_query', $meta_query );
                    break;
                case 'past':
                    $meta_query = array(
                        array(
                            'key' => '_start_event_datetime',
                            'value' => $now,
                            'type' => 'numeric',
                            'compare' => '<'
                        )
                    );
                    $query->set( 'meta_query', $meta_query );
                    break;
                case 'all':
                    break;
                default:
                    $meta_query = array(
                        array(
                            'key' => '_start_event_datetime',
                            'value' => $now,
                            'type' => 'numeric',
                            'compare' => '>'
                        )
                    );
                    $query->set( 'meta_query', $meta_query );
                    break;
            }
        }
    }
    
    /**
     * Filter dropdown for upcoming and past events
     * @return type
     */
    public function events_filter_dropdown(){
        $type = 'event';
        if ($_GET['post_type'] != $type) return;

        $values = array(
            __('Upcoming events', 'bandpress') => 'upcoming', 
            __('Past events', 'bandpress') => 'past',
            __('All events', 'bandpress') => 'all',
        );
        ?>
        <select name="EVENTS_TIME_FILTER">
        <?php
            $current_v = isset($_GET['EVENTS_TIME_FILTER'])? $_GET['EVENTS_TIME_FILTER']:'upcoming';
            foreach ($values as $label => $value) {
                printf
                (
                    '<option value="%s"%s>%s</option>',
                    $value,
                    $value == $current_v? ' selected="selected"':'',
                    $label
                );
            }
        ?>
        </select>
        <?php
    }
    
    /**
     * Adds an event count column to the users panel
     * @param array $cols
     * @return string
     */
    public function users_events_column( $cols ) {
        $cols['user_events'] = 'Events';   
        return $cols;
    }
    
    /**
     * Fills the event count column in the user panel
     * @global type $wpdb
     * @param type $value
     * @param type $column_name
     * @param type $id
     * @return type
     */
    public function user_events_column_value( $value, $column_name, $id ) {
        if( $column_name == 'user_events' ) {
            global $wpdb;
            $count = (int)$wpdb->get_var($wpdb->prepare(
              "SELECT COUNT(ID) FROM $wpdb->posts WHERE 
               post_type = 'event' AND post_status = 'publish' AND post_author = %d",
               $id
            ));
            if($count > 0){
                $url = admin_url('edit.php?post_type=event&author=' . $id);
                return '<a href=' . $url . '>' . $count . '</a>';
            } else {
                return $count;
            }
        }
    }
    
    /**
     * Inserts events into the main loop
     * @param type $query
     * @return type
     */
    public function events_in_loop( $query ) {
        if ( is_home() && $query->is_main_query() )
            $query->set( 'post_type', array( 'post', 'event') );
        return $query;
    }
    
    /**
     * Adds event capabilities to the admin role
     */
    public function add_capabilities(){
        $role = get_role('administrator');
        
        $role->add_cap( 'edit_event' ); 
        $role->add_cap( 'edit_events' ); 
        $role->add_cap( 'edit_other_events' ); 
        $role->add_cap( 'publish_events' ); 
        $role->add_cap( 'read_event' ); 
        $role->add_cap( 'read_private_events' ); 
        $role->add_cap( 'delete_event' ); 
    }
    
    /**
     * Make columns sortable
     * @param type $columns
     * @return type
     */
    public function add_sortable_columns($columns) {        
        $columns['startdate'] = 'startdate';
        //$columns['enddate'] = 'enddate';
        //$columns['entry_fee'] = 'entry_fee';
        //$columns['location'] = 'location';
        return $columns;
    }
    
    /**
     * Sorting our columns
     */
    public function sort_columns( $vars ) { 
        global $pagenow;
        
        if($pagenow == 'edit.php' && is_admin() && $_GET['post_type'] == 'event'){
            if(isset( $vars['orderby'] )){
                if ('startdate' == $vars['orderby'] ) {
                    $vars = array_merge( $vars, array(
                        'meta_key' => '_start_event_datetime',
                        'orderby' => 'meta_value_num',
                    ) );
                } else 
                if ('entry_fee' == $vars['orderby'] ) {
                    $vars = array_merge( $vars, array(
                        'meta_key' => '_event_entry_fee',
                        'orderby' => 'meta_value'
                    ) );
                }
                if ('location' == $vars['orderby'] ) {
                    $vars = array_merge( $vars, array(
                        'meta_key' => '_event_location',
                        'orderby' => 'meta_value'
                    ) );
                }
            } else {
                $vars = array_merge( $vars, array(
                    'meta_key' => '_start_event_datetime',
                    'orderby' => 'meta_value_num',
                    'order' => 'asc'
                ));
            }
        }

        return $vars;
    }
    
    /**
     * Admin column content
     * @global type $wpdb
     * @param type $column_name
     * @param type $id
     */
    public function fill_columns($column_name, $id) {
        global $wpdb;
        switch ($column_name) {
            case 'startdate':
                $date = BPEvents::get_event_date($id);
                echo "<b>" . date_i18n( "j M y", $date->getTimeStamp()) . "</b>";
                break;
            case 'enddate':
                $date = BPEvents::get_event_end_date($id);
                if($date != null){
                    $datestring = "<b>" . date_i18n( "j M y", $date->getTimeStamp()) . "</b>";
                    echo $datestring;
                } else {
                    echo "-";
                }
                break;
            case 'entry_fee':
                $fee = get_post_meta($id, '_event_entry_fee', true);
                if($fee != ''){
                    echo $fee;
                } else {
                    echo "-";
                }
                break;
            case 'location':
                $location = get_post_meta($id, '_event_location', true);
                if($location != ''){
                    echo $location;
                } else {
                    echo "-";
                }
                break;
            default:
                break;
        } // end switch
    }
    
    /**
     * Admin column headers
     * @param type $columns
     * @return type
     */
    public function add_columns($columns){
        $options = get_option('bp_options');
        
        $new_columns['cb'] = '<input type="checkbox" />';
     
        $new_columns['startdate'] = __('Start date', 'bandpress');
        
        if($options['use_end_date'] == "yes"){
            $new_columns['enddate'] = __('End date', 'bandpress');
        }
        
        $new_columns['title'] = _x('Title', 'column name', 'bandpress');
        
        if($options['use_entry_fee'] == "yes"){
            $new_columns['entry_fee'] = __('Entry fee', 'bandpress');
        }
        
        if($options['use_location'] == "yes"){
            $new_columns['location'] = __('Location', 'bandpress');
        }
        
        $new_columns['author'] = __('Author', 'bandpress');

        return $new_columns;
    }
    
    /**
     * Create the post type Event
     */
    public function create_post_type(){
        register_post_type( 'event',
            array(
                'labels' => array(
                    'name' => __( 'Events', 'bandpress' ),
                    'singular_name' => __( 'Event', 'bandpress' ),
                    'add_new' => __('Add New', 'bandpress'),
                    'add_new_item' => __('Add New Event', 'bandpress'),
                    'edit_item' => __('Edit Event', 'bandpress'),
                    'new_item' => __('New Event', 'bandpress'),
                    'all_items' => __('All Events', 'bandpress'),
                    'view_item' => __('View Event', 'bandpress'),
                    'search_items' => __('Search Events', 'bandpress'),
                    'not_found' =>  __('Events not found', 'bandpress'),
                    'not_found_in_trash' => __('No events found in trash', 'bandpress'), 
                    'menu_name' => __('Events', 'bandpress')
                ),
                'public' => true,
                'has_archive' => true,
                'publicly_queryable' => true,
                'show_ui' => true, 
                'show_in_menu' => true, 
                'query_var' => true,
                'rewrite' => array( 'slug' => 'event' ),
                'capability_type' => 'event',
                'capabilities' => array(
                    'edit_post' => 'edit_event',
                    'edit_posts' => 'edit_events',
                    'edit_others_posts' => 'edit_other_events',
                    'publish_posts' => 'publish_events',
                    'read_post' => 'read_event',
                    'read_private_posts' => 'read_private_events',
                    'delete_post' => 'delete_event'
                ),
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array( 'title', 'editor', 'thumbnail', 'comments' )
            )
	);
    }
    
    /**
     * Add date and location metabox
     */
    public function add_date_metaboxes(){
	add_meta_box( 'event_properties', __('Event properties', 'bandpress'), array($this, 'event_properties_metabox'), 'event', 'side', 'default', array( 'id' => '_properties') );
    }
    
    /**
     * Create the datetime picker metabox
     * @global type $post
     * @global type $wp_locale
     * @param type $post
     * @param type $args
     */
    function event_properties_metabox($post, $args){
        $metabox_id = $args['args']['id'];
        global $post, $wp_locale;
        
        // Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'ep_eventposts_nonce' );
        
        $datetime = BPEvents::get_event_date();
        $date = date_i18n( "d-m-Y", $datetime->getTimeStamp());
        $time = date_i18n( "H:i", $datetime->getTimeStamp());
        
        if($date == "05-05-1905"){
            $date = date_i18n("d-m-Y");
        }
        
        $enddatetime = BPEvents::get_event_end_date();
        $enddate = date_i18n( "d-m-Y", $enddatetime->getTimeStamp());
        $endtime = date_i18n( "H:i", $enddatetime->getTimeStamp());
        
        $event_location = get_post_meta( $post->ID, '_event_location', true );
        $event_entry_fee = get_post_meta( $post->ID, '_event_entry_fee', true );
        $options = get_option('bp_options');
        
        include('bp-event-datetime-metabox.php');
    }
    
    /**
     * Save data from the datepickers and timepickers
     * @param type $post_id
     * @param type $post
     * @return type
     */
    function event_save_datetimepickers($post_id, $post){
        $options = get_option('bp_options');

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;

	if ( !isset( $_POST['ep_eventposts_nonce'] ) )
		return;

	if ( !wp_verify_nonce( $_POST['ep_eventposts_nonce'], plugin_basename( __FILE__ ) ) )
		return;

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ) )
		return;

	// OK, we're authenticated: we need to find and save the data
	// We'll put it into an array to make it easier to loop though
        
        if($options['use_end_date'] == "yes"){
            $metabox_ids = array( '_start', '_end' );
        } else {
            $metabox_ids = array('_start');
        }
        
        // Handle datetime pickers meta
        foreach ($metabox_ids as $key ) {
            $date = $_POST[$key . '_date'];
            $time = $_POST[$key . '_time'];
            
            if($date != null || $date == ""){
                $date = split("-", $date);
                $day = $date[0];
                $month = $date[1];
                $year = $date[2];
                $year = ($year <= 0 ) ? date('Y') : $year;
                $month = ($month <= 0 ) ? date('n') : $month;
                $day = sprintf('%02d',$day);
                $day = ($day > 31 ) ? 31 : $day;
                $day = ($day <= 0 ) ? date('j') : $day;
            }
            if($time != null || $date ==""){
                $time = split(":", $time);
                $hour = $time[0];
                $minute = $time[1];
                $hour = sprintf('%02d',$hour);
                $hour = ($hour > 23 ) ? 23 : $hour;
                $minute = sprintf('%02d',$minute);
                $minute = ($minute > 59 ) ? 59 : $minute;
            }

            $events_meta[$key . '_year'] = $year;
            $events_meta[$key . '_month'] = $month;
            $events_meta[$key . '_day'] = $day;
            $events_meta[$key . '_hour'] = $hour;
            $events_meta[$key . '_minute'] = $minute;
            $events_meta[$key . '_event_datetime'] = $year . $month . $day . $hour . $minute;
        }
        
        // Save Locations Meta
        if($options['use_event_location'] == "yes"){
            $events_meta['_event_location'] = $_POST['_event_location'];
        }
        if($options['use_entry_fee'] == "yes"){
            $events_meta['_event_entry_fee'] = $_POST['_event_entry_fee'];
        }

	// Add values of $events_meta as custom fields
	foreach ( $events_meta as $key => $value ) { // Cycle through the $events_meta array!
            if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
            $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
            if ( get_post_meta( $post->ID, $key, FALSE ) ) { // If the custom field already has a value
                update_post_meta( $post->ID, $key, $value );
            } else { // If the custom field doesn't have a value
                add_post_meta( $post->ID, $key, $value );
            }
            if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
	}
    }
    
    /**
     * Retrieve the event date
     * @global type $post
     */
    public static function get_event_date() {
        global $post;
        
        $year = (int) get_post_meta($post->ID, '_start_year', true);
        $month = (int) get_post_meta($post->ID, '_start_month', true);
        $day = (int) get_post_meta($post->ID, '_start_day', true);
        $hour = (int) get_post_meta($post->ID, '_start_hour', true);
        $minute = (int) get_post_meta($post->ID, '_start_minute', true);
        
        $datetime = new DateTime_52();
        $datetime->setDate($year, $month, $day);
        $datetime->setTime($hour, $minute);
        
        return $datetime;
    }
    
    /**
     * End date of event
     * @global type $post
     * @return \DateTime
     */
    public static function get_event_end_date() {
        global $post;
        
        $year = (int) get_post_meta($post->ID, '_end_year', true);
        $month = (int) get_post_meta($post->ID, '_end_month', true);
        $day = (int) get_post_meta($post->ID, '_end_day', true);
        $hour = (int) get_post_meta($post->ID, '_end_hour', true);
        $minute = (int) get_post_meta($post->ID, '_end_minute', true);
        
        $datetime = new DateTime_52();
        $datetime->setDate($year, $month, $day);
        $datetime->setTime($hour, $minute);
        
        return $datetime;
    }
    
    public static function get_the_month_abbr($month) {
        global $wp_locale;
        for ( $i = 1; $i < 13; $i = $i +1 ) {
            if ( $i == $month )
                $monthabbr = $wp_locale->get_month_abbrev( $wp_locale->get_month( $i ) );
            }
        return $monthabbr;
    }
    
    /**
    * Customize Event Query using Post Meta
    * 
    * @link http://www.billerickson.net/customize-the-wordpress-query/
    * @param object $query data
    */
    function event_query( $query ) {
        // http://codex.wordpress.org/Function_Reference/current_time
        $now = date('YmdHi');

        global $wp_the_query;

        if ( $wp_the_query === $query && !is_admin() && is_post_type_archive( 'event' ) ) {
            $meta_query = array(
                array(
                    'key' => '_start_event_datetime',
                    'value' => $now,
                    'type' => 'numeric',
                    'compare' => '>'
                )
            );
            $query->set( 'meta_query', $meta_query );
            $query->set( 'orderby', 'meta_value_num' );
            $query->set( 'meta_key', '_start_event_datetime' );
            $query->set( 'order', 'ASC' );
            $options = get_option('bp_options');
            $query->set( 'posts_per_page', $options['events_per_page'] );
        }
    }
}
}
?>