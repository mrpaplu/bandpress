<?php
/**
 * PBLocalization takes care of the plugin being localizable.
 *
 * @author Kay van Bree
 */
if(!class_exists('BPLocalization')){
class BPLocalization {
    public function __construct(){
        add_action('plugins_loaded', array($this, 'activate_localization'));
    }
    
    /*
     * Activate localization
     */
    public function activate_localization(){
        $plugin_dir = dirname(plugin_basename(__FILE__)) . '../../languages';
        load_plugin_textdomain('bandpress', false, $plugin_dir);
    }
}
}
?>