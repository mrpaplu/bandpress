<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bp-event-categories
 *
 * @author Kay
 */
if(!class_exists('BPEventCategories')){
class BPEventCategories {
    public function __construct(){
        add_action( 'init', array($this, 'add_taxonomy') );
    }
    
    public function add_taxonomy() {
        
        $labels = array(
            'name'                => __( 'Categories' ),
            'singular_name'       => __( 'Category' ),
            'search_items'        => __( 'Search Categories' ),
            'all_items'           => __( 'All Categories' ),
            'parent_item'         => __( 'Parent Category' ),
            'parent_item_colon'   => __( 'Parent Category:' ),
            'edit_item'           => __( 'Edit Category' ), 
            'update_item'         => __( 'Update Category' ),
            'add_new_item'        => __( 'Add New Category' ),
            'new_item_name'       => __( 'New Category Name' ),
            'menu_name'           => __( 'Category', 'bandpress' )
        );
        
        $args = array(
            'hierarchical'        => true,
            'labels'              => $labels,
            'show_ui'             => true,
            'show_admin_column'   => true,
            'query_var'           => true,
            'rewrite' => array( 'slug' => 'event_cat' )
        );
	// create a new taxonomy
	register_taxonomy(
            'event_categories',
            'event',
            
            $args
	);
    }   
}
}
?>